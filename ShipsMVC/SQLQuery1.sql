INSERT INTO userstoredb.dbo.Ships
(Name, UserId, BaseShipId, YearOfBuilding, FullHp, MaxSpeed, RotationSpeed, Manageability, SailArea, SailMass, SideThickness, 
Description, MassCenter, MastAngle, MastHp, ShipHeigth, ShipLength, ShipWidth, ShipTypeId) VALUES
('Halcon', 1, 1, 1840, 3000, 14.7, 4, 3, 2000, 300, 7, 'Halcon was a fast clipper-schooner with 12 guns', 0, 0, 45, 28, 20, 7, 1),
('Essex', 1, 2, 1799, 7500, 11.4, 2, 1.76, 4000, 800, 18, 'Essex was an American frigate with 36 guns', 0, 0, 120, 50, 42, 12, 5),
('Rattlesnake', 1, 3, 1822, 5700, 12.5, 2.5, 2.3, 3000, 650, 11, 'Rattlesnake was  a 28-gun corvette', 0, 0, 70, 43, 35, 10, 4),
('Beagle', 1, 4, 1818, 4000, 13.5, 3.2, 2.8, 2700, 570, 9, 'Beagle is 18-gun sloop', 0, 0, 65, 35, 28, 9, 3);

UPDATE userstoredb.dbo.Ships
SET ShipTypeId=3
WHERE Name='Beagle'

DELETE FROM userstoredb.dbo.WoodTypes

DELETE FROM userstoredb.dbo.Ships

INSERT INTO userstoredb.dbo.WoodPartTypes(PartTypeName) VALUES
('Carcass'),('Planking'),('Mast'),('Deck');

INSERT INTO userstoredb.dbo.Woods(Name) VALUES
('Fir'), ('Oak'), ('Teak'), ('Live-oak');

INSERT INTO userstoredb.dbo.ShipType(Name) VALUES
('Schooner'), ('Brig'), ('Sloop'), ('Corvette'), ('Frigate'), ('Battleship 3rd rate'), ('Battleship 2nd rate'), ('Battleship 1st rate');

INSERT INTO userstoredb.dbo.WoodTypes
(WoodId, WoodPartTypeId, HpBonus, MastHpBonus, SailAreaBonus, SailMassBonus, SideThicknessBonus, SpeedBonus, SpeedRotationBonus) VALUES 
(1, 1, -0.1, 0, 0, 0,-0.1, 0.07, 0.05),
(2, 1, 0.03, 0, 0, 0, 0.01, 0.03, 0.03),
(3, 1, 0.1, 0, 0, 0, 0.1, -0.05, -0.02),
(4, 1, 0.17, 0, 0, 0, 0.15, -0.1, -0.1),
(1, 2, -0.05, 0, 0, 0,-0.15, 0.03, 0.05),
(2, 2, 0.03, 0, 0, 0, 0.05, 0.01, 0.02),
(3, 2, 0.05, 0, 0, 0, 0.1, -0.02, -0.02),
(4, 2, 0.1, 0, 0, 0, 0.2, -0.08, -0.07),
(1, 3, 0, -0.13, -0.05, -0.1, 0, 0.03, 0.05),
(2, 3, 0, 0.02, 0.05, 0.05, 0, 0.01, 0.02),
(3, 3, 0, 0.04, 0.07, 0.1, 0, -0.02, -0.01),
(4, 3, 0, 0.07, 0.09, 0.15, 0, -0.06, -0.05),
(1, 4, -0.07, 0, 0, 0, 0, 0.02, 0.01),
(2, 4, 0.01, 0, 0, 0, 0, 0.01, -0.01),
(3, 4, 0.02, 0, 0, 0, 0, -0.01, -0.02),
(4, 4, 0.03, 0, 0, 0, 0, -0.02, -0.03);

UPDATE userstoredb.dbo.Ships
SET BlueprintPath='Blueprints/halcon.jpg'
WHERE Id=7;

UPDATE userstoredb.dbo.Ships
SET BlueprintPath='Blueprints/essex.jpg'
WHERE Id=8;

UPDATE userstoredb.dbo.Ships
SET BlueprintPath='Blueprints/rattlesnake.jpg'
WHERE Id=9;

UPDATE userstoredb.dbo.Ships
SET BlueprintPath='Blueprints/beagle.jpg'
WHERE Id=10;

INSERT INTO userstoredb.dbo.SubscribeDescriptions(UserId, SubscribeId, IsAutomatic, LastConfirm) VALUES
(1, 1, 0, '2019-02-12'),
(2, 1, 0, '2019-02-12')

INSERT INTO userstoredb.dbo.LimitType(Name) VALUES
('RowsLinit'),
('Published')

INSERT INTO userstoredb.dbo.Limit(TypeId, Value) VALUES
(1, '5'),
(1, '10'),
(1, '15'),
(2, 'True')

INSERT userstoredb.dbo.SubscribeToLimit(LimitId, SubscribeId) VALUES
(2, 1),
(6, 1),
(3, 2),
(6, 2),
(4, 3),
(5, 3)

DELETE FROM userstoredb.dbo.SubscribeToLimit
WHERE LimitId=5

DELETE FROM userstoredb.dbo.Limit
WHERE Id=5



