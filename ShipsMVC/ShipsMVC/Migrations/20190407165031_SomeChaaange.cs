﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ShipsMVC.Migrations
{
    public partial class SomeChaaange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_SubscribeDescriptions_SubscribeDescriptionId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_SubscribeDescriptionId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "SubscribeDescriptionId",
                table: "Users");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SubscribeDescriptionId",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_SubscribeDescriptionId",
                table: "Users",
                column: "SubscribeDescriptionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_SubscribeDescriptions_SubscribeDescriptionId",
                table: "Users",
                column: "SubscribeDescriptionId",
                principalTable: "SubscribeDescriptions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
