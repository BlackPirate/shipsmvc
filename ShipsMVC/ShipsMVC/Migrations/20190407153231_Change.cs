﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ShipsMVC.Migrations
{
    public partial class Change : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsAutomatic",
                table: "Subscribes");

            migrationBuilder.AddColumn<bool>(
                name: "IsAutomaticSubscribe",
                table: "Users",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsAutomaticSubscribe",
                table: "Users");

            migrationBuilder.AddColumn<bool>(
                name: "IsAutomatic",
                table: "Subscribes",
                nullable: false,
                defaultValue: false);
        }
    }
}
