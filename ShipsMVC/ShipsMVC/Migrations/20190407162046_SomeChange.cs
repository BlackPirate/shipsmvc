﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ShipsMVC.Migrations
{
    public partial class SomeChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Subscribes_SubscribeId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_SubscribeId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "SubscribeId",
                table: "Users");

            migrationBuilder.CreateIndex(
                name: "IX_SubscribeDescriptions_SubscribeId",
                table: "SubscribeDescriptions",
                column: "SubscribeId");

            migrationBuilder.AddForeignKey(
                name: "FK_SubscribeDescriptions_Subscribes_SubscribeId",
                table: "SubscribeDescriptions",
                column: "SubscribeId",
                principalTable: "Subscribes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SubscribeDescriptions_Subscribes_SubscribeId",
                table: "SubscribeDescriptions");

            migrationBuilder.DropIndex(
                name: "IX_SubscribeDescriptions_SubscribeId",
                table: "SubscribeDescriptions");

            migrationBuilder.AddColumn<int>(
                name: "SubscribeId",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_SubscribeId",
                table: "Users",
                column: "SubscribeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Subscribes_SubscribeId",
                table: "Users",
                column: "SubscribeId",
                principalTable: "Subscribes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
