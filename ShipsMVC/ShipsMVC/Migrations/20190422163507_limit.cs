﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShipsMVC.Migrations
{
    public partial class limit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LimitId",
                table: "Subscribes",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "LimitType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LimitType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Limit",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TypeId = table.Column<int>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Limit", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Limit_LimitType_TypeId",
                        column: x => x.TypeId,
                        principalTable: "LimitType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Subscribes_LimitId",
                table: "Subscribes",
                column: "LimitId");

            migrationBuilder.CreateIndex(
                name: "IX_Limit_TypeId",
                table: "Limit",
                column: "TypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Subscribes_Limit_LimitId",
                table: "Subscribes",
                column: "LimitId",
                principalTable: "Limit",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subscribes_Limit_LimitId",
                table: "Subscribes");

            migrationBuilder.DropTable(
                name: "Limit");

            migrationBuilder.DropTable(
                name: "LimitType");

            migrationBuilder.DropIndex(
                name: "IX_Subscribes_LimitId",
                table: "Subscribes");

            migrationBuilder.DropColumn(
                name: "LimitId",
                table: "Subscribes");
        }
    }
}
