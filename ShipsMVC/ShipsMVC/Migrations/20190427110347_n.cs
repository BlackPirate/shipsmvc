﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ShipsMVC.Migrations
{
    public partial class n : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Limit_LimitType_TypeId",
                table: "Limit");

            migrationBuilder.DropForeignKey(
                name: "FK_SubscribeToLimit_Limit_LimitId",
                table: "SubscribeToLimit");

            migrationBuilder.DropForeignKey(
                name: "FK_SubscribeToLimit_Subscribes_SubscribeId",
                table: "SubscribeToLimit");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SubscribeToLimit",
                table: "SubscribeToLimit");

            migrationBuilder.DropPrimaryKey(
                name: "PK_LimitType",
                table: "LimitType");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Limit",
                table: "Limit");

            migrationBuilder.RenameTable(
                name: "SubscribeToLimit",
                newName: "SubscribeToLimits");

            migrationBuilder.RenameTable(
                name: "LimitType",
                newName: "LimitTypes");

            migrationBuilder.RenameTable(
                name: "Limit",
                newName: "Limits");

            migrationBuilder.RenameIndex(
                name: "IX_SubscribeToLimit_SubscribeId",
                table: "SubscribeToLimits",
                newName: "IX_SubscribeToLimits_SubscribeId");

            migrationBuilder.RenameIndex(
                name: "IX_SubscribeToLimit_LimitId",
                table: "SubscribeToLimits",
                newName: "IX_SubscribeToLimits_LimitId");

            migrationBuilder.RenameIndex(
                name: "IX_Limit_TypeId",
                table: "Limits",
                newName: "IX_Limits_TypeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SubscribeToLimits",
                table: "SubscribeToLimits",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_LimitTypes",
                table: "LimitTypes",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Limits",
                table: "Limits",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Limits_LimitTypes_TypeId",
                table: "Limits",
                column: "TypeId",
                principalTable: "LimitTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SubscribeToLimits_Limits_LimitId",
                table: "SubscribeToLimits",
                column: "LimitId",
                principalTable: "Limits",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SubscribeToLimits_Subscribes_SubscribeId",
                table: "SubscribeToLimits",
                column: "SubscribeId",
                principalTable: "Subscribes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Limits_LimitTypes_TypeId",
                table: "Limits");

            migrationBuilder.DropForeignKey(
                name: "FK_SubscribeToLimits_Limits_LimitId",
                table: "SubscribeToLimits");

            migrationBuilder.DropForeignKey(
                name: "FK_SubscribeToLimits_Subscribes_SubscribeId",
                table: "SubscribeToLimits");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SubscribeToLimits",
                table: "SubscribeToLimits");

            migrationBuilder.DropPrimaryKey(
                name: "PK_LimitTypes",
                table: "LimitTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Limits",
                table: "Limits");

            migrationBuilder.RenameTable(
                name: "SubscribeToLimits",
                newName: "SubscribeToLimit");

            migrationBuilder.RenameTable(
                name: "LimitTypes",
                newName: "LimitType");

            migrationBuilder.RenameTable(
                name: "Limits",
                newName: "Limit");

            migrationBuilder.RenameIndex(
                name: "IX_SubscribeToLimits_SubscribeId",
                table: "SubscribeToLimit",
                newName: "IX_SubscribeToLimit_SubscribeId");

            migrationBuilder.RenameIndex(
                name: "IX_SubscribeToLimits_LimitId",
                table: "SubscribeToLimit",
                newName: "IX_SubscribeToLimit_LimitId");

            migrationBuilder.RenameIndex(
                name: "IX_Limits_TypeId",
                table: "Limit",
                newName: "IX_Limit_TypeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SubscribeToLimit",
                table: "SubscribeToLimit",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_LimitType",
                table: "LimitType",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Limit",
                table: "Limit",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Limit_LimitType_TypeId",
                table: "Limit",
                column: "TypeId",
                principalTable: "LimitType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SubscribeToLimit_Limit_LimitId",
                table: "SubscribeToLimit",
                column: "LimitId",
                principalTable: "Limit",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SubscribeToLimit_Subscribes_SubscribeId",
                table: "SubscribeToLimit",
                column: "SubscribeId",
                principalTable: "Subscribes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
