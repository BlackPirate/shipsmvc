﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShipsMVC.Migrations
{
    public partial class AddSubscribe : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SubscribeId",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Subscribes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Priority = table.Column<int>(nullable: false),
                    IsAutomatic = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscribes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_SubscribeId",
                table: "Users",
                column: "SubscribeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Subscribes_SubscribeId",
                table: "Users",
                column: "SubscribeId",
                principalTable: "Subscribes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Subscribes_SubscribeId",
                table: "Users");

            migrationBuilder.DropTable(
                name: "Subscribes");

            migrationBuilder.DropIndex(
                name: "IX_Users_SubscribeId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "SubscribeId",
                table: "Users");
        }
    }
}
