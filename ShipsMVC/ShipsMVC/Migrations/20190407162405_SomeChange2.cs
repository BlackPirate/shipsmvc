﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ShipsMVC.Migrations
{
    public partial class SomeChange2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_SubscribeDescriptions_SubscribeDescriptionId1",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_SubscribeDescriptionId1",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "SubscribeDescriptionId1",
                table: "Users");

            migrationBuilder.CreateIndex(
                name: "IX_Users_SubscribeDescriptionId",
                table: "Users",
                column: "SubscribeDescriptionId");

            migrationBuilder.CreateIndex(
                name: "IX_SubscribeDescriptions_UserId",
                table: "SubscribeDescriptions",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_SubscribeDescriptions_Users_UserId",
                table: "SubscribeDescriptions",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_SubscribeDescriptions_SubscribeDescriptionId",
                table: "Users",
                column: "SubscribeDescriptionId",
                principalTable: "SubscribeDescriptions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SubscribeDescriptions_Users_UserId",
                table: "SubscribeDescriptions");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_SubscribeDescriptions_SubscribeDescriptionId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_SubscribeDescriptionId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_SubscribeDescriptions_UserId",
                table: "SubscribeDescriptions");

            migrationBuilder.AddColumn<int>(
                name: "SubscribeDescriptionId1",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_SubscribeDescriptionId1",
                table: "Users",
                column: "SubscribeDescriptionId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_SubscribeDescriptions_SubscribeDescriptionId1",
                table: "Users",
                column: "SubscribeDescriptionId1",
                principalTable: "SubscribeDescriptions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
