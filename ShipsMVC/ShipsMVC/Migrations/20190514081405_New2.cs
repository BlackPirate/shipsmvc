﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ShipsMVC.Migrations
{
    public partial class New2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DaysCount",
                table: "SubscribeDescriptions");

            migrationBuilder.AddColumn<int>(
                name: "Cost",
                table: "Subscribes",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DaysCount",
                table: "Subscribes",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Cost",
                table: "Subscribes");

            migrationBuilder.DropColumn(
                name: "DaysCount",
                table: "Subscribes");

            migrationBuilder.AddColumn<int>(
                name: "DaysCount",
                table: "SubscribeDescriptions",
                nullable: false,
                defaultValue: 0);
        }
    }
}
