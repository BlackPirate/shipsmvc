﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShipsMVC.Migrations
{
    public partial class New : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ShipType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShipType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WoodPartTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PartTypeName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WoodPartTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Woods",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Woods", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Email = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    RoleId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WoodTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    WoodId = table.Column<int>(nullable: true),
                    WoodPartTypeId = table.Column<int>(nullable: true),
                    SpeedBonus = table.Column<float>(nullable: false),
                    SpeedRotationBonus = table.Column<float>(nullable: false),
                    HpBonus = table.Column<float>(nullable: false),
                    SideThicknessBonus = table.Column<float>(nullable: false),
                    MastHpBonus = table.Column<float>(nullable: false),
                    SailAreaBonus = table.Column<float>(nullable: false),
                    SailMassBonus = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WoodTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WoodTypes_Woods_WoodId",
                        column: x => x.WoodId,
                        principalTable: "Woods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_WoodTypes_WoodPartTypes_WoodPartTypeId",
                        column: x => x.WoodPartTypeId,
                        principalTable: "WoodPartTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Ships",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BaseShipId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    YearOfBuilding = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: true),
                    ShipTypeId = table.Column<int>(nullable: true),
                    BlueprintPath = table.Column<string>(nullable: true),
                    MaxSpeed = table.Column<float>(nullable: false),
                    RotationSpeed = table.Column<float>(nullable: false),
                    Manageability = table.Column<float>(nullable: false),
                    FullHp = table.Column<int>(nullable: false),
                    SideThickness = table.Column<int>(nullable: false),
                    MastHp = table.Column<int>(nullable: false),
                    MastAngle = table.Column<int>(nullable: false),
                    ShipLength = table.Column<float>(nullable: false),
                    ShipWidth = table.Column<float>(nullable: false),
                    ShipHeigth = table.Column<float>(nullable: false),
                    MassCenter = table.Column<float>(nullable: false),
                    SailArea = table.Column<int>(nullable: false),
                    SailMass = table.Column<int>(nullable: false),
                    CarcassWoodId = table.Column<int>(nullable: true),
                    PlankingWoodId = table.Column<int>(nullable: true),
                    DeckWoodId = table.Column<int>(nullable: true),
                    MastWoodId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ships", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Ships_WoodTypes_CarcassWoodId",
                        column: x => x.CarcassWoodId,
                        principalTable: "WoodTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ships_WoodTypes_DeckWoodId",
                        column: x => x.DeckWoodId,
                        principalTable: "WoodTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ships_WoodTypes_MastWoodId",
                        column: x => x.MastWoodId,
                        principalTable: "WoodTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ships_WoodTypes_PlankingWoodId",
                        column: x => x.PlankingWoodId,
                        principalTable: "WoodTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ships_ShipType_ShipTypeId",
                        column: x => x.ShipTypeId,
                        principalTable: "ShipType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ships_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Name" },
                values: new object[] { 1, "admin" });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Name" },
                values: new object[] { 2, "user" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Email", "Password", "RoleId" },
                values: new object[] { 1, "admin@mail.ru", "admin", 1 });

            migrationBuilder.CreateIndex(
                name: "IX_Ships_CarcassWoodId",
                table: "Ships",
                column: "CarcassWoodId");

            migrationBuilder.CreateIndex(
                name: "IX_Ships_DeckWoodId",
                table: "Ships",
                column: "DeckWoodId");

            migrationBuilder.CreateIndex(
                name: "IX_Ships_MastWoodId",
                table: "Ships",
                column: "MastWoodId");

            migrationBuilder.CreateIndex(
                name: "IX_Ships_PlankingWoodId",
                table: "Ships",
                column: "PlankingWoodId");

            migrationBuilder.CreateIndex(
                name: "IX_Ships_ShipTypeId",
                table: "Ships",
                column: "ShipTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Ships_UserId",
                table: "Ships",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_RoleId",
                table: "Users",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_WoodTypes_WoodId",
                table: "WoodTypes",
                column: "WoodId");

            migrationBuilder.CreateIndex(
                name: "IX_WoodTypes_WoodPartTypeId",
                table: "WoodTypes",
                column: "WoodPartTypeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Ships");

            migrationBuilder.DropTable(
                name: "WoodTypes");

            migrationBuilder.DropTable(
                name: "ShipType");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Woods");

            migrationBuilder.DropTable(
                name: "WoodPartTypes");

            migrationBuilder.DropTable(
                name: "Roles");
        }
    }
}
