﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ShipsMVC.Migrations
{
    public partial class AddSubDesc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsAutomaticSubscribe",
                table: "Users");

            migrationBuilder.AddColumn<int>(
                name: "SubscribeDescriptionId",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SubscribeDescriptionId1",
                table: "Users",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "SubscribeDescriptions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    SubscribeId = table.Column<int>(nullable: false),
                    IsAutomatic = table.Column<bool>(nullable: false),
                    LastConfirm = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubscribeDescriptions", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_SubscribeDescriptionId1",
                table: "Users",
                column: "SubscribeDescriptionId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_SubscribeDescriptions_SubscribeDescriptionId1",
                table: "Users",
                column: "SubscribeDescriptionId1",
                principalTable: "SubscribeDescriptions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_SubscribeDescriptions_SubscribeDescriptionId1",
                table: "Users");

            migrationBuilder.DropTable(
                name: "SubscribeDescriptions");

            migrationBuilder.DropIndex(
                name: "IX_Users_SubscribeDescriptionId1",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "SubscribeDescriptionId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "SubscribeDescriptionId1",
                table: "Users");

            migrationBuilder.AddColumn<bool>(
                name: "IsAutomaticSubscribe",
                table: "Users",
                nullable: false,
                defaultValue: false);
        }
    }
}
