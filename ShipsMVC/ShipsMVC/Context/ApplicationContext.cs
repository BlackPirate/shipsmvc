﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShipsMVC.Models
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Subscribe> Subscribes { get; set; }
        public DbSet<SubscribeDescription> SubscribeDescriptions { get; set; }
        public DbSet<Ship> Ships { get; set; }
        public DbSet<ShipType> ShipType { get; set; }
        public DbSet<Wood> Woods { get; set; }
        public DbSet<WoodType> WoodTypes { get; set; }
        public DbSet<WoodPartType> WoodPartTypes { get; set; }
        public DbSet<Limit> Limits { get; set; }
        public DbSet<LimitType> LimitTypes { get; set; }
        public DbSet<SubscribeToLimit> SubscribeToLimits { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            string adminRoleName = "admin";
            string userRoleName = "user";

            string adminEmail = "admin@mail.ru";
            string adminPassword = "admin";

            Role adminRole = new Role { Id = 1, Name = adminRoleName };
            Role userRole = new Role { Id = 2, Name = userRoleName };
            User adminUser = new User { Id = 1, Email = adminEmail, Password = adminPassword, RoleId = adminRole.Id };

            modelBuilder.Entity<Role>().HasData(new Role[] { adminRole, userRole });
            modelBuilder.Entity<User>().HasData(new User[] { adminUser });
            base.OnModelCreating(modelBuilder);
        }

        /// <summary>
        /// Overrided Find with Initialize
        /// </summary>
        /// <param name="context"></param>
        /// <param name="keyValues"></param>
        /// <returns></returns>
        public Ship Find(params object[] keyValues)
        {
            return base.Find<Ship>(keyValues).Initialize(this);
        }

        //public bool Add<Ship>(object entity)
        //{
        //    var ship = (Ship)entity;
        //    var e=base.Add(entity);
        //    return true;
        //}
    }
}
