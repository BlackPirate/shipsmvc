﻿using Microsoft.AspNetCore.Http;
using ShipsMVC.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShipsMVC.Models
{
    // Описание модели корабля
    public class Ship
    {
        // Аттрибут Display означает, что свойство отображается для пользователя. Параметр - отображаемое имя
        // Аттрибут Changalbe означает, что свойство может изменяться пользователем
        public int Id { get; set; }
        public int BaseShipId { get; set; }
        [Display(Name = "Название")]
        public string Name { get; set; }
        [Display(Name = "Год постройки")]
        public int YearOfBuilding { get; set; }
        [Display(Name = "Описание")]
        public string Description { get; set; }

        public int? UserId { get; set; }
        public User User { get; set; }

        public int? ShipTypeId { get; set; }
        [Display(Name = "Тип корабля")]
        public ShipType ShipType { get; set; }

        public string BlueprintPath { get; set; }

        [Display(Name = "Максимальная скорость")]
        public float MaxSpeed { get; set; }
        [Display(Name = "Скорость поворота")]
        public float RotationSpeed { get; set; }
        [Display(Name = "Маневренность")]
        public float Manageability { get; set; }
        [Display(Name = "Прочность")]
        public int FullHp { get; set; }
        [Display(Name = "Толщина борта")]
        public int SideThickness { get; set; }
        [Display(Name = "Прочность мачт")]
        public int MastHp { get; set; }

        [Display(Name = "Уровень наклона мачты")]
        [Changable]
        public int MastAngle { get; set; }

        [Display(Name = "Длина")]
        public float ShipLength { get; set; }
        [Display(Name = "Ширина")]
        public float ShipWidth { get; set; }
        [Display(Name = "Высота")]
        public float ShipHeigth { get; set; }

        [Display(Name = "Центр массы")]
        [Changable]
        public float MassCenter { get; set; }

        [Display(Name = "Площадь парусов")]
        public int SailArea { get; set; }
        [Display(Name = "Масса парусов")]
        public int SailMass { get; set; }

        public int? CarcassWoodId { get; set; }
        [Display(Name = "Остов")]
        [Changable]
        public WoodType CarcassWood { get; set; }

        public int? PlankingWoodId { get; set; }
        [Display(Name = "Обшивка")]
        [Changable]
        public WoodType PlankingWood { get; set; }

        public int? DeckWoodId { get; set; }
        [Display(Name = "Палуба")]
        [Changable]
        public WoodType DeckWood { get; set; }

        public int? MastWoodId { get; set; }
        [Display(Name = "Мачты")]
        [Changable]
        public WoodType MastWood { get; set; }
    }

    static class ShipExtentions
    {
        /// <summary>
        /// Обязательна при извлечении кораблей из БД
        /// Инициализирует поля дерева, типа судна и хоязина корабля по соответствующему им Id
        /// </summary>
        /// <param name="ships"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static IEnumerable<Ship> Initialize(this IEnumerable<Ship> ships, ApplicationContext context)
        {
            foreach (var ship in ships)
            {
                Initialize(ship, context);
            }
            return ships;
        }

        /// <summary>
        /// Обязательна при извлечении корабля из БД
        /// Инициализирует поля дерева, типа судна и хоязина корабля по соответствующему им Id
        /// </summary>
        /// <param name="ships"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static Ship Initialize(this Ship ship, ApplicationContext context)
        {
            if (ship.UserId != null)
            {
                ship.User = context.Users.Find(ship.UserId);
            }
            if (ship.ShipTypeId != null)
            {
                ship.ShipType = context.ShipType.Find(ship.ShipTypeId);
            }
            if (ship.MastWoodId != null)
            {
                ship.MastWood = context.WoodTypes.Find(ship.MastWoodId);
                ship.MastWood.Wood = context.Woods.Find(ship.MastWood.WoodId);
                ship.MastWood.WoodPartType = context.WoodPartTypes.Find(ship.MastWood.WoodPartTypeId);
            }
            if (ship.DeckWoodId != null)
            {
                ship.DeckWood = context.WoodTypes.Find(ship.DeckWoodId);
                ship.DeckWood.Wood = context.Woods.Find(ship.DeckWood.WoodId);
                ship.DeckWood.WoodPartType = context.WoodPartTypes.Find(ship.DeckWood.WoodPartTypeId);
            }
            if (ship.CarcassWoodId != null)
            {
                ship.CarcassWood = context.WoodTypes.Find(ship.CarcassWoodId);
                ship.CarcassWood.Wood = context.Woods.Find(ship.CarcassWood.WoodId);
                ship.CarcassWood.WoodPartType = context.WoodPartTypes.Find(ship.CarcassWood.WoodPartTypeId);
            }
            if (ship.PlankingWoodId != null)
            {
                ship.PlankingWood = context.WoodTypes.Find(ship.PlankingWoodId);
                ship.PlankingWood.Wood = context.Woods.Find(ship.PlankingWood.WoodId);
                ship.PlankingWood.WoodPartType = context.WoodPartTypes.Find(ship.PlankingWood.WoodPartTypeId);
            }
            return ship;
        }

        /// <summary>
        /// Изменяет угол наклона относительно 0 - нормального положения мачты
        /// </summary>
        /// <param name="ship">корабль, который изменяем</param>
        /// <param name="angle">Угол в градусах - положительный или отрицательный</param>
        /// <returns>Успешно ли произошла операция</returns>
        public static void SetMastAngle(this Ship ship, int angle, Ship baseShip)
        {
            ship.MastAngle = angle;
            // наклон в корму уменьшает нагрузку на мачту - она может нести больше парусов, слегка увеличиваz hp
            ship.SailMass -= (baseShip.SailMass * angle) / 100;
            ship.MastHp -= (baseShip.MastHp * angle) / 500;
            // однако в то же время он худшает управления гиком при слабом ветре и работу стакселя, теряется скорость поворота.
            ship.RotationSpeed += (baseShip.RotationSpeed * angle * 0.05f) / 100;
            ship.Manageability += (baseShip.Manageability * angle * 0.1f) / 100;
            // наклон мачты в нос имеет противоположное влияние
        }

        /// <summary>
        /// Изменение дифферента
        /// </summary>
        /// <param name="ship"></param>
        /// <param name="center">новое положение дифферента</param>
        /// <param name="baseShip">модель корабля в базовой таблице</param>
        public static void SetMassCenter(this Ship ship, int center, Ship baseShip)
        {
            ship.Manageability = baseShip.Manageability + center / 900f;
            ship.SailMass = baseShip.SailMass - center / 15;
        }

        /// <summary>
        /// Клонирует судно
        /// </summary>
        /// <param name="ship"></param>
        /// <returns>новый экземпляр Ship</returns>
        public static Ship Clone(this Ship ship)
        {
            return new Ship()
            {
                BaseShipId = ship.BaseShipId,
                Name = ship.Name,
                YearOfBuilding = ship.YearOfBuilding,
                Description = ship.Description,
                UserId = ship.UserId,
                User = ship.User,
                ShipTypeId = ship.ShipTypeId,
                ShipType = ship.ShipType,
                BlueprintPath = ship.BlueprintPath,
                MaxSpeed = ship.MaxSpeed,
                RotationSpeed = ship.RotationSpeed,
                Manageability = ship.Manageability,
                FullHp = ship.FullHp,
                SideThickness = ship.SideThickness,
                MastHp = ship.MastHp,
                MastAngle = ship.MastAngle,
                ShipLength = ship.ShipLength,
                ShipWidth = ship.ShipWidth,
                ShipHeigth = ship.ShipHeigth,
                MassCenter = ship.MassCenter,
                SailArea = ship.SailArea,
                SailMass = ship.SailMass,
                CarcassWood = ship.CarcassWood,
                CarcassWoodId = ship.CarcassWoodId,
                PlankingWood = ship.PlankingWood,
                PlankingWoodId = ship.PlankingWoodId,
                MastWood = ship.MastWood,
                MastWoodId = ship.MastWoodId,
                DeckWood = ship.DeckWood,
                DeckWoodId = ship.DeckWoodId
            };
        }
    }
}