﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShipsMVC.Models
{
    public class WoodPartType
    {
        public int Id { get; set; }
        public string PartTypeName { get; set; }

        public List<WoodType> WoodTypes { get; set; }
        public WoodPartType()
        {
            WoodTypes = new List<WoodType>();
        }
    }
}
