﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShipsMVC.Models
{
    // Описание дерева и его бонусов
    public class WoodType
    {
        public int Id { get; set; }

        public int? WoodId { get; set; }
        public Wood Wood { get; set; }

        public int? WoodPartTypeId { get; set; }
        public WoodPartType WoodPartType { get; set; }

        public float SpeedBonus { get; set; }
        public float SpeedRotationBonus { get; set; }
        public float HpBonus { get; set; }
        public float SideThicknessBonus { get; set; }

        public float MastHpBonus { get; set; }
        public float SailAreaBonus { get; set; }
        public float SailMassBonus { get; set; }

        public override string ToString()
        {
            return Wood.Name;
        }
    }
}
