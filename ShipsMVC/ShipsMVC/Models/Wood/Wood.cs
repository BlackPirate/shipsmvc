﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShipsMVC.Models
{
    public class Wood
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<WoodType> WoodTypes { get; set; }
        public Wood()
        {
            WoodTypes = new List<WoodType>();
        }
    }
}
