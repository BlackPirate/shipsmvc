﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShipsMVC.Models
{
    public class ShipType
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<Ship> Ships { get; set; }

        public ShipType()
        {
            Ships = new List<Ship>();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
