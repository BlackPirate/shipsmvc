﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShipsMVC.Models
{
    public class LimitType
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<Limit> Limits { get; set; }

        public LimitType()
        {
            Limits = new List<Limit>();
        }
    }
}
