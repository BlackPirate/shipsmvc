﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShipsMVC.Models
{
    public class SubscribeDescription
    {
        public int Id { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        public int SubscribeId { get; set; }
        public Subscribe Subscribe { get; set; }

        public bool IsAutomatic { get; set; }
        public DateTime LastConfirm { get; set; }
    }
}
