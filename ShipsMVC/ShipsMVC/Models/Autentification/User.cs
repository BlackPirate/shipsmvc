﻿using ShipsMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShipsMVC.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Money { get; set; }

        public int? RoleId { get; set; }
        public Role Role { get; set; }

        public List<Ship> Ships { get; set; }
        public List<SubscribeDescription> SubscribeDescriptions { get; set; }

        public User()
        {
            Ships = new List<Ship>();
            SubscribeDescriptions = new List<SubscribeDescription>();
        }
    }
}
