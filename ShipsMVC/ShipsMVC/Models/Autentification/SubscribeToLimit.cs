﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShipsMVC.Models
{
    public class SubscribeToLimit
    {
        public int Id { get; set; }

        public int? LimitId { get; set; }
        public Limit Limit { get; set; }

        public int? SubscribeId { get; set; }
        public Subscribe Subscribe { get; set; }
    }
}
