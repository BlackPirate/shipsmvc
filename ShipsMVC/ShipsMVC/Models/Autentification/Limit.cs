﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShipsMVC.Models
{
    public class Limit
    {
        public int Id { get; set; }

        public int? TypeId { get; set; }
        public LimitType Type { get; set; }

        public string Value { get; set; }

        public string FullName { get { return $"{Type.Name} {Value}"; } }

        public List<SubscribeToLimit> SubscribeToLimits { get; set; }

        public Limit()
        {
            SubscribeToLimits = new List<SubscribeToLimit>();
        }
    }

    public static class Extention
    {
        /// <summary>
        /// Определение того, может ли данный корабль быть добавлен в БД
        /// </summary>
        /// <param name="user"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static bool CanAdd(this User user, ApplicationContext context)
        {
            var subscribeDesc = context.SubscribeDescriptions.Where(x => x.UserId == user.Id).First();
            var subscribe = context.Subscribes.Find(subscribeDesc.SubscribeId);
            var subToLimit = context.SubscribeToLimits
                .Where(x => x.SubscribeId == subscribe.Id)
                .ToList();

            var limits = subToLimit
                .Select(x => context.Limits.Find(x.LimitId).Initialise(context))
                .ToList();
            var limit=limits
                .Where(x => x.Type.Name == "RowsLinit")
                .First();
            var value = int.Parse(limit.Value);
            var realCount = context.Ships.Where(x => x.UserId == user.Id).Count();
            return realCount < value;
        }

        public static List<Limit> Initialize(this List<Limit> limits, ApplicationContext context)
        {
            foreach(var limit in limits)
            {
                limit.Initialise(context);
            }
            return limits;
        }

        public static Limit Initialise(this Limit limit, ApplicationContext context)
        {
            limit.Type = context.LimitTypes.Find(limit.TypeId);
            return limit;
        }
    }
}
