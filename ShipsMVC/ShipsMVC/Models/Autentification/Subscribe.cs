﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShipsMVC.Models
{
    public class Subscribe
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Priority { get; set; }

        // На сколько дней подписка
        public int DaysCount { get; set; }
        public int Cost { get; set; }

        public List<SubscribeDescription> SubscribeDescriptions { get; set; }
        public List<SubscribeToLimit> SubscribeToLimits { get; set; }
        
        public Subscribe()
        {
            SubscribeDescriptions = new List<SubscribeDescription>();
            SubscribeToLimits = new List<SubscribeToLimit>();
        }
    }
}
