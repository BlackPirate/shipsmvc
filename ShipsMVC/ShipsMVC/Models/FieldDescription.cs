﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShipsMVC.Models
{
    public class FieldDescription
    {
        public string Name;
        public string Value;
        public string OriginalName;
    }
}
