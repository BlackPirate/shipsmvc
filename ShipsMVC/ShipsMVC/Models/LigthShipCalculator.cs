﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShipsMVC.Models
{
    public class LigthShipCalculator : IShipCalculator
    {
        public void Calculate(Ship ship, Ship baseShip)
        {
            ship.MaxSpeed = CalculateSpeed(ship, baseShip);
            ship.RotationSpeed = CalculateRotationSpeed(ship, baseShip);
            ship.FullHp = (int)CalculateHp(ship, baseShip);
            ship.SideThickness = (int)CalculateThickness(ship, baseShip);
            ship.MastHp = (int)(baseShip.MastHp * (1 + ship.MastWood.MastHpBonus));
            ship.SailArea = (int)(baseShip.SailArea * (1 + ship.MastWood.SailAreaBonus));
            ship.SailMass = (int)(baseShip.SailMass * (1 + ship.MastWood.SailMassBonus));
        }

        #region BonusCalculations
        private static float CalculateSpeed(Ship ship, Ship baseShip)
        {
            var speed = baseShip.MaxSpeed;
            speed += baseShip.MaxSpeed * ship.CarcassWood.SpeedBonus;
            speed += baseShip.MaxSpeed * ship.PlankingWood.SpeedBonus;
            speed += baseShip.MaxSpeed * ship.MastWood.SpeedBonus;
            speed += baseShip.MaxSpeed * ship.DeckWood.SpeedBonus;
            return speed;
        }

        private static float CalculateRotationSpeed(Ship ship, Ship baseShip)
        {
            var speed = baseShip.RotationSpeed;
            speed += baseShip.RotationSpeed * ship.CarcassWood.SpeedRotationBonus;
            speed += baseShip.RotationSpeed * ship.PlankingWood.SpeedRotationBonus;
            speed += baseShip.RotationSpeed * ship.MastWood.SpeedRotationBonus;
            speed += baseShip.RotationSpeed * ship.DeckWood.SpeedRotationBonus;
            return speed;
        }

        private static float CalculateHp(Ship ship, Ship baseShip)
        {
            float hp = baseShip.FullHp;
            hp += baseShip.FullHp * ship.CarcassWood.HpBonus;
            hp += baseShip.FullHp * ship.PlankingWood.HpBonus;
            hp += baseShip.FullHp * ship.DeckWood.HpBonus;
            return hp;
        }

        private static float CalculateThickness(Ship ship, Ship baseShip)
        {
            float thickness = baseShip.SideThickness;
            thickness += baseShip.SideThickness * ship.CarcassWood.SideThicknessBonus;
            thickness += baseShip.SideThickness * ship.PlankingWood.SideThicknessBonus;
            return thickness;
        }
        #endregion
    }
}
