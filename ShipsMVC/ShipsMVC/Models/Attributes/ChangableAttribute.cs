﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShipsMVC.Models.Attributes
{
    public class ChangableAttribute : Attribute
    {
        public int MinValue;
        public int MaxValue;
    }
}
