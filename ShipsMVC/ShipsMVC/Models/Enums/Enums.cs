﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShipsMVC.Models
{
    public enum SortType
    {
        Name,
        NameDes,
        Year,
        YearDes
    }
}
