﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShipsMVC.Models
{
    interface IShipCalculator
    {
        void Calculate(Ship ship, Ship baseShip);
    }
}
