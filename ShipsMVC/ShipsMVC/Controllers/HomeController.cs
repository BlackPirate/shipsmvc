﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ShipsMVC.Models;
using ShipsMVC.Models.Attributes;


namespace ShipsMVC.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationContext context;

        public HomeController(ApplicationContext context)
        {
            this.context = context;
            HomeCtrlHelper.context = context;
        }

        public IActionResult Start(SortType sortType, string search)
        {
            // Сортировка по имени и году (меняется на противоположное)
            ViewBag.NameSortPar = sortType == SortType.Name ? SortType.NameDes : SortType.Name;
            ViewBag.YearSortPar = sortType == SortType.Year ? SortType.YearDes : SortType.Year;
            return View(HomeCtrlHelper.GetShipsWithUserId(1, sortType, search));
        }

        [Authorize(Roles = "user, admin")]
        public IActionResult UserShips(int Id, SortType sortType = 0, string search = null)
        {
            return View(HomeCtrlHelper.GetShipsWithUserId(Id, sortType, search));
        }

        /// <summary>
        /// Получение всех отображаемых характеристик корабля
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IActionResult Details(int id)
        {
            var ship = context.Find(id);
            ViewBag.ShipId = id;
            ViewBag.Blueprint = ship.BlueprintPath;
            List<FieldDescription> fields =HomeCtrlHelper.GetVisibleFields(ship);
            ViewBag.Fields = fields;
            
            // Является ли данный пользователь владельцем данного корабля
            if (User.Identity.IsAuthenticated)
            {
                var user = context.Users.Find(int.Parse(User.FindFirst(x => x.Type == ClaimTypes.NameIdentifier).Value));
                ViewBag.IsUserOwned = user.Id == context.Ships.Find(id).UserId;
                ViewBag.CanUserAdd = user.CanAdd(context);
            }
            else
            {
                ViewBag.IsUserOwned = false;
                ViewBag.CanUserAdd = false;
            }
            
            return View();
        }

        public IActionResult Settings(int baseShipId)
        {
            var ship = context.Find(baseShipId);
            ViewBag.ShipId = baseShipId;
            ViewBag.Wood = new SelectList(context.Woods, "Id", "Name");
            List<FieldDescription> fields = HomeCtrlHelper.GetFieldsWithValue(ship);
            ViewBag.Fields = fields;
            return View();
        }

        public IActionResult Save(int shipId, int carcassWood, int plankingWood, int mastWood, int deckWood, int massCenter, int mastAngle)
        {
            var ship = context.Find(shipId).Clone();
            var baseShip = context.Ships.Find(ship.BaseShipId);
            SaveShip(ship, baseShip, carcassWood, plankingWood, mastWood, deckWood, massCenter, mastAngle);

            ViewBag.NewFields = HomeCtrlHelper.GetVisibleFields(ship);
            ViewBag.OldFields = HomeCtrlHelper.GetVisibleFields(baseShip);
            return View();
        }

        public FileResult Download(string path)
        {
            byte[] mas = System.IO.File.ReadAllBytes(path);
            string file_type = "application/jpg";
            string file_name = "blueprint";
            return File(mas, file_type, file_name);
        }

        public void SaveShip(Ship ship, Ship baseShip, int carcassWood, int plankingWood, int mastWood, int deckWood, int massCenter, int mastAngle)
        {
            ship.CarcassWoodId = context.WoodTypes.Where(x => x.WoodId == carcassWood && x.WoodPartType.PartTypeName == "Carcass").First().Id;
            ship.PlankingWoodId = context.WoodTypes.Where(x => x.WoodId == plankingWood && x.WoodPartType.PartTypeName == "Planking").First().Id;
            ship.MastWoodId = context.WoodTypes.Where(x => x.WoodId == mastWood && x.WoodPartType.PartTypeName == "Mast").First().Id;
            ship.DeckWoodId = context.WoodTypes.Where(x => x.WoodId == deckWood && x.WoodPartType.PartTypeName == "Deck").First().Id;

            ship.Initialize(context);

            ship.SetMassCenter(massCenter, baseShip);
            ship.SetMastAngle(mastAngle, baseShip);
            new LigthShipCalculator().Calculate(ship, baseShip);

            if (ship.UserId == 1)
            {
                ship.UserId = int.Parse(User.FindFirst(x => x.Type == ClaimTypes.NameIdentifier).Value);
                context.Ships.Add(ship);
            }
            else context.Ships.Update(ship);

            //if (ship.UserId == 1)
            //{
            //    ship.UserId = int.Parse(ClaimsPrincipal.Current.FindFirst(x => x.Type == ClaimTypes.NameIdentifier).Value);
            //    context.Ships.Add(ship);
            //}
            //else context.Ships.Update(ship);

            context.SaveChanges();
        }

        //public IActionResult GetMoney(int userId)
        //{
        //    var user = context.Users.Find(userId);
        //    user.Money += 1000;
        //    context.Users.Update(user);
        //    context.SaveChanges();
        //    return RedirectToAction(nameof( Start));
        //}

        //public ActionResult UploadImage(Object sender, HttpPostedFileBase Image)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        if (Image != null)
        //        {
        //            string fileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
        //            FileUpload1.PostedFile.SaveAs(Server.MapPath("~/Content/") + fileName);
        //        }
        //    }
        //}
    }
}
