﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ShipsMVC.Models;

namespace ShipsMVC.Controllers
{
    public class AdminController : Controller
    {
        private ApplicationContext context;

        public AdminController(ApplicationContext context)
        {
            this.context = context;
        }

        [Authorize(Roles = "admin")]
        public IActionResult Limit()
        {
            ViewBag.Limits = context.Limits.ToList().Initialize(context);
            ViewBag.Types = new SelectList(context.LimitTypes.ToList(), "Id", "Name");
            return View();
        }

        [Authorize(Roles = "admin")]
        public IActionResult AddLimit(int limitTypeId, string limitValue)
        {
            if (limitTypeId == 0 || limitValue == null)
                return RedirectToAction(nameof(Limit));
            var limit = new Limit();
            limit.TypeId = limitTypeId;
            limit.Value = limitValue;
            var isRepit = context.Limits.Where(x => x.TypeId == limit.TypeId && x.Value == limit.Value).Count() != 0;
            if (!isRepit)
            {
                context.Limits.Add(limit);
                context.SaveChanges();
            }
            return RedirectToAction(nameof(Limit));
        }

        [Authorize(Roles = "admin")]
        public IActionResult Subscribe(int limitCount = 0)
        {
            ViewBag.LimitAll = context.LimitTypes.Count();
            ViewBag.LimitCount = limitCount;
            ViewBag.Subscribe = context.Subscribes.ToList();
            ViewBag.Limits = new SelectList(context.Limits.ToList().Initialize(context), "Id", "FullName");
            return View();
        }

        [Authorize(Roles = "admin")]
        public IActionResult GetLimitCount(int limitCount)
        {
            ViewBag.LimitCount = limitCount;
            return RedirectToAction(nameof(Subscribe));
        }

        [Authorize(Roles = "admin")]
        public IActionResult SubscribeDetails(int subId)
        {
            var limitsId = context.SubscribeToLimits.Where(x => x.SubscribeId == subId).Select(x=>x.LimitId).ToList();
            ViewBag.Limits = context.Limits.Where(x => limitsId.Contains(x.Id)).ToList().Initialize(context);
            return View();
        }

        [Authorize(Roles = "admin")]
        public IActionResult AddSubscribe(string subName, int subPriority, int cost, int time, params int[] limitsId)
        {
            var limits = context.Limits
                .Where(x => limitsId.Contains(x.Id))
                .ToList()
                .Initialize(context)
                .Select(x => x.Type.Name);
            var isRepitType = limits.ToHashSet().Count != limits.Count();
            if (!isRepitType)
            {
                var subscribe = new Subscribe();
                subscribe.Name = subName;
                subscribe.Priority = subPriority;
                context.Subscribes.Add(subscribe);
                context.SaveChanges();

                foreach (var limit in limitsId)
                {
                    var subToLimit = new SubscribeToLimit();
                    subToLimit.LimitId = limit;
                    subToLimit.SubscribeId = context.Subscribes.Where(x => x.Name == subName).First().Id;
                    context.SubscribeToLimits.Add(subToLimit);
                }

                context.SaveChanges();
            }
            return RedirectToAction(nameof(Subscribe));
        }

        [Authorize(Roles = "admin")]
        public IActionResult CreateNewShip()
        {
            ViewBag.Fields = HomeCtrlHelper.GetVisibleNonchangableFields();
            return View();
        }

        [Authorize(Roles = "admin")]
        public IActionResult SaveShipInDatabase(
            [Bind("Name,YearOfBuilding,Description,MaxSpeed,RotationSpeed,Manageability," +
            "FullHp,SideThickness,MastHp,ShipLength,ShipWidth,ShipHeigth,SailArea,SailMass")] Ship ship)
        {
            ship.UserId = 1;
            context.Ships.Add(ship);
            context.SaveChanges();
            return RedirectToAction(nameof(HomeController.Start));
        }


        private List<int?> RepeatsSubscribe(int[] limitsId)
        {
            return context.SubscribeToLimits.GroupBy(x => x.SubscribeId, x => x.LimitId)
            .Where(g => limitsId.All(y => g.Contains(y)))
            .Select(x => x.Key)
            .ToList();
        }
    }
}