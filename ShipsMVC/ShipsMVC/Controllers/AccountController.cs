﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using ShipsMVC.Models.ViewModels; 
using ShipsMVC.Models; 
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Linq;
using System;

namespace ShipsMVC.Controllers
{
    public class AccountController : Controller
    {
        private ApplicationContext _context;
        public AccountController(ApplicationContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await _context.Users.FirstOrDefaultAsync(u => u.Email == model.Email);
                if (user == null)
                {
                    // добавляем пользователя в бд
                    user = new User { Email = model.Email, Password = model.Password, Money = 5000 };
                    Role userRole = await _context.Roles.FirstOrDefaultAsync(r => r.Name == "user");
                    if (userRole != null)
                        user.Role = userRole;

                    _context.Users.Add(user);
                    await _context.SaveChangesAsync();

                    var us = _context.Users.Where(x => x.Email == model.Email).First();
                    var subDes = new SubscribeDescription();
                    subDes.LastConfirm = DateTime.Now;
                    subDes.SubscribeId = _context.Subscribes.Where(x => x.Name == "Base").First().Id;
                    subDes.UserId = us.Id;
                    _context.SubscribeDescriptions.Add(subDes);

                    await _context.SaveChangesAsync();

                    await Authenticate(user); 

                    return RedirectToAction("Start", "Home");
                }
                else
                    ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                User user = await _context.Users
                    .Include(u => u.Role)
                    .FirstOrDefaultAsync(u => u.Email == model.Email && u.Password == model.Password);
                if (user != null)
                {
                    // Проверка истечения подписки и откат ее к базовой
                    var subDesc = _context.SubscribeDescriptions.Where(x => x.UserId == user.Id).First();
                    var sub = _context.Subscribes.Find(subDesc.SubscribeId);
                    if ((DateTime.Now - subDesc.LastConfirm).Days > sub.DaysCount)
                    {
                        subDesc.SubscribeId = _context.Subscribes.Where(x => x.Priority == 0).First().Id;
                        _context.SubscribeDescriptions.Update(subDesc);
                        _context.SaveChanges();
                    }

                    await Authenticate(user);
                    return RedirectToAction("Start", "Home");
                }
                ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return View(model);
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account");
        }

        public IActionResult ImproveSubscribe()
        {
            var curUser= _context.Users.Find(int.Parse(User.FindFirst(x => x.Type == ClaimTypes.NameIdentifier).Value));
            var curSubscribe = _context.Subscribes.Find(_context.SubscribeDescriptions.Where(x => x.UserId == curUser.Id).First().SubscribeId);
            var subscribes = _context.Subscribes.Where(x => x.Priority > curSubscribe.Priority).OrderBy(x => x);
            ViewBag.Message = (subscribes.Count()==0) ? "У вас уже максимальный тип подписки" : "Улучшить подписку до:";
            ViewBag.Subscribes = subscribes;
            return View();
        }

        public IActionResult PayToSubscribe(int id)
        {
            ViewBag.Id = id;
            return View();
        }

        public IActionResult PayRequest(int id, string isAutomatic)
        {
            var curUser = _context.Users.Find(int.Parse(User.FindFirst(x => x.Type == ClaimTypes.NameIdentifier).Value));
            var curSubDesc = _context.SubscribeDescriptions.Where(x => x.UserId == curUser.Id).First();
            curSubDesc.IsAutomatic = isAutomatic == "on";
            curSubDesc.SubscribeId = id;
            curSubDesc.LastConfirm = DateTime.Now;
            _context.Users.Update(curUser);
            _context.SaveChanges();
            return RedirectToAction("Start", "Home");
        }

        private async Task Authenticate(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role?.Name),
                // Клэйм для хранения Id пользователя
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
            };
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }
    }
}