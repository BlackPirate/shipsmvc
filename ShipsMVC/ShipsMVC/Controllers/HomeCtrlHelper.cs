﻿using ShipsMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using ShipsMVC.Models.Attributes;

namespace ShipsMVC.Controllers
{
    public static class HomeCtrlHelper
    {
        public static ApplicationContext context;


        /// <summary>
        /// Получение всех свойсв класса Ship и атрибутов их отображений
        /// </summary>
        /// <param name="ship"></param>
        /// <returns></returns>
        public static List<FieldDescription> GetVisibleFields(Ship ship)
        {
            return typeof(Ship).GetProperties()
                .Where(x => x.GetCustomAttributes(false).Where(z => z is DisplayAttribute).Count() == 1)
                .Select(x => new FieldDescription()
                {
                    Name = ((DisplayAttribute)x.GetCustomAttributes(false).First()).Name,
                    Value = x.GetValue(ship)?.ToString(),
                    OriginalName = x.Name
                }).ToList();
        }



        /// <summary>
        /// Получение кораблей заданного пользователя
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="sortType"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        public static IEnumerable<Ship> GetShipsWithUserId(int userId, SortType sortType, string search)
        {
            var ships = context.Ships.Where(x => x.UserId == userId).Initialize(context);
            // Поиск
            if (search != null)
            {
                ships = ships.Where(ship => ship.Name.Contains(search));
            }
            // Сортировка
            switch (sortType)
            {
                case SortType.YearDes:
                    ships = ships.OrderByDescending(ship => ship.YearOfBuilding);
                    break;
                case SortType.NameDes:
                    ships = ships.OrderByDescending(ship => ship.Name);
                    break;
                case SortType.Year:
                    ships = ships.OrderBy(ship => ship.YearOfBuilding);
                    break;
                default:
                    ships = ships.OrderBy(ship => ship.Name);
                    break;
            }
            return ships.ToList();
        }

        public static List<FieldDescription> GetVisibleNonchangableFields()
        {
            return typeof(Ship).GetProperties()
                .Where(x => x.GetCustomAttributes(false).Where(z => z is DisplayAttribute).Count() == 1)
                .Where(x => x.GetCustomAttributes(false).Where(z => z is ChangableAttribute).Count() == 0)
                .Select(x => new FieldDescription()
                {
                    Name = ((DisplayAttribute)x.GetCustomAttributes(false).First()).Name,
                    OriginalName = x.Name
                })
                .ToList();
        }


        public static List<FieldDescription> GetFieldsWithValue(Ship ship)
        {
            return typeof(Ship).GetProperties()
                .Where(x => x.GetCustomAttributes(false).Where(z => z is DisplayAttribute).Count() == 1)
                .Where(x => x.GetCustomAttributes(false).Where(z => z is ChangableAttribute).Count() == 0)
                .Select(x => new FieldDescription()
                {
                    Name = ((DisplayAttribute)x.GetCustomAttributes(false).First()).Name,
                    Value = x.GetValue(ship)?.ToString()
                })
                .ToList();
        }
    }
}
